// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	for(size_t i=0;i<disparos.size();i++)
		delete disparos[i];
	disparos.clear();

	dato->accion=10;

	printf("Conexión finalizada.\n");
	socket_servidor.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,525,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	for(int i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	for(int i=0;i<disparos.size();i++)	
		disparos[i]->Mueve(0.025f);

	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	for(int j=0;j<disparos.size();j++)
	{
		if(jugador1.Colision(disparos[j]))
		{
			disparos.erase(disparos.begin()+j);
			jugador1.y1=jugador1.y1*0.8;
			jugador1.y2=jugador1.y2*0.8;
			break;
		}
		else if(jugador2.Colision(disparos[j]))
		{
			disparos.erase(disparos.begin()+j);
			jugador2.y1=jugador2.y1*0.8;
			jugador2.y2=jugador2.y2*0.8;
			break;
		}
		else if (fondo_izq.Colision(disparos[j]))
		{
			disparos.erase(disparos.begin()+j);
			break;
		}
		else if (fondo_dcho.Colision(disparos[j]))
		{
			disparos.erase(disparos.begin()+j);
			break;
		}
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		recompensa2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		recompensa1++;
	}

	dato->esfera=esfera;
	dato->raqueta1=jugador1;
	if(dato->accion==1)
		OnKeyboardDown('w',0,0);

	else if(dato->accion==-1)
		OnKeyboardDown('s',0,0);

	dato->raqueta2=jugador2;
	if(jugador2.tiempo_espera>=200)
	{
		if(dato->accion2==1)
			OnKeyboardDown('o',0,0);

		else if(dato->accion2==-1)
			OnKeyboardDown('l',0,0);
	}

	jugador1.espera--;
	jugador2.espera--;

	jugador2.tiempo_espera=jugador2.tiempo_espera+1;

	char cad[200];
	if(socket_servidor.Receive(cad,200)==-2) desconectar=1;
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %d %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2, &recompensa1, &recompensa2,  &jugador2.tiempo_espera);

	if(puntos1==3||puntos2==3||desconectar==1) 
	{
		OnKeyboardDown(' ',0,0);
		exit(0);
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 'q':
		{	
			if(jugador1.espera<=0&&recompensa1>0)
			{
				disparos.push_back(new Disparo(jugador1.x1+0.3f,(jugador1.y1+jugador1.y2)/2.0f,4.0f,0.0f));
				jugador1.espera=20;
				recompensa1--;
			}	
			break;
		}
		case 'p':
		{
			if(jugador2.espera<=0&&recompensa2>0)

			{
				disparos.push_back(new Disparo(jugador2.x1-0.3f,(jugador2.y1+jugador2.y2)/2.0f,-4.0f,0.0f));
				jugador2.espera=20;
				recompensa2--;
			}
			break;
		}
	}
	char cad[100];
	sprintf(cad,"%c %d %d",key,x,y);
	socket_servidor.Send(cad,100);
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//memoria compartida
	fd2=open("/home/sergio/sii-50240/practica1/datos",O_CREAT|O_RDWR,0777);
	write(fd2,&datos,sizeof(datos));
	dato=(DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
	close(fd2);

	//Sockets
	char cad[100];
	printf("Introduce nombre: ");
	fgets(cad,100,stdin);
	socket_servidor.Connect((char *)"127.0.0.1",1100);
	socket_servidor.Send(cad,100);
	desconectar=0;
}
