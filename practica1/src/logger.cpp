#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[])
{
	mkfifo("/home/sergio/sii-50240/practica1/fifo",0777);
	int fd=open("/home/sergio/sii-50240/practica1/fifo",O_RDONLY);
	if(fd<0) perror("Error al abrir pipe");	

	while(1)
	{
		char cad[100];
		read(fd,cad,sizeof(cad));
		printf("%s\n", cad);

		if(strcmp(cad, "Fin del programa. Partida no finalizada.")==0) break;
		else if(strcmp(cad, "Fin del programa. ¡JUGADOR 1 GANA!")==0) break;
		else if(strcmp(cad, "Fin del programa. ¡JUGADOR 2 GANA!")==0) break;
	}


	close(fd);
	unlink("/home/sergio/sii-50240/practica1/fifo");
	return 0;
}
