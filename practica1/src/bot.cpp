#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main(int argc, char* argv[])
{
	DatosMemCompartida *dato;
	int fd2=open("/home/sergio/sii-50240/practica1/datos",O_RDWR);
	dato=(DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
	close(fd2);
	
	while(1)
	{
		float pos=(dato->raqueta1.y1+dato->raqueta1.y2)/2;
		float pos2=(dato->raqueta2.y1+dato->raqueta2.y2)/2;
		if(pos>dato->esfera.centro.y)
			dato->accion=-1;
		else if(pos<dato->esfera.centro.y)
			dato->accion=1;
		else dato->accion=0;

		if(pos2>dato->esfera.centro.y)
			dato->accion2=-1;
		else if(pos2<dato->esfera.centro.y)
			dato->accion2=1;
		else dato->accion2=0;

		usleep(25000);

		if(dato->accion==10) break;
	}
	return 0;
}
