#include "Disparo.h"
#include "glut.h"

Disparo::Disparo()
{
	radio=0.2f;
	posicion.x=1.0f;
	posicion.y=1.0f;
	velocidad.x=1.0f;
	velocidad.y=1.0f;
}

Disparo::Disparo(float px,float py,float vx,float vy)
{
	radio=0.2f;
	posicion.x=px;
	posicion.y=py;
	velocidad.x=vx;
	velocidad.y=vy;
}

Disparo::~Disparo()
{
}

void Disparo::Dibuja()
{
	glColor3ub(50,255,50);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(posicion.x,posicion.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}


void Disparo::Mueve(float t)
{
	posicion.x=posicion.x+velocidad.x*t;
	posicion.y=posicion.y+velocidad.y*t;
}

