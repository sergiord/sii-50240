// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Disparo.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	int espera;
	int tiempo_espera;
	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
