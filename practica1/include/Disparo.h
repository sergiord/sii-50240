#ifndef DISPARO_H
#define DISPARO_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "Vector2D.h"

class Disparo
{
public:
	Vector2D posicion;
	Vector2D velocidad;
	float radio;
	Disparo();
	Disparo(float px,float py,float vx,float vy);
	virtual ~Disparo();
	void Dibuja();
	void Mueve(float t);

};

#endif // !defined(AFX_DISPARO_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
