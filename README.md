Controles:

- Jugador 1: 'w': Mover raqueta hacia arriba.
             's': Mover raqueta hacia abajo.
             'q': Disparar.

- Jugador 2: 'o': Mover raqueta hacia arriba.
             'l': Mover raqueta hacia abajo.
             'p': Disparar.

- "ESPACIO": Cerrar programa.
